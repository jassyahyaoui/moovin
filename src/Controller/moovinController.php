<?php
/**
 * Created by PhpStorm.
 * User: Jasser
 * Date: 15/11/2018
 * Time: 21:38
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class moovinController extends AbstractController
{
    /**
     * @Route("/", name="moovin_index")
     */
    public function index()
    {
        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/contact", options={"expose"=true}, name="moovin_contact_email")
     */
    public function contact(Request $request, \Swift_Mailer $mailer)
    {
        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $contact = $request->request->all();

        $message = (new \Swift_Message($contact['subject']))
            ->setFrom(array($contact['email'] => 'Moovin contact'))
            ->setTo('dheker.hamami@gmail.com')
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'emails/contact.html.twig',
                    array(
                        'name' => $contact['name'],
                        'email' => $contact['email'],
                        'phone' => $contact['phone'] ?? 'pas de numero saisie',
                        'sector' => $contact['sector'],
                        'subject' => $contact['subject'],
                        'message' => $contact['message'],
                    )
                ),
                'text/html'
            );

        $mailer->send($message);

        return new JsonResponse('email send', 200);
    }

    /**
     * @Route("/devis", options={"expose"=true}, name="moovin_devist_email")
     */
    public function devis(Request $request, \Swift_Mailer $mailer)
    {
        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $devis = $request->request->all();

        $message = (new \Swift_Message('Approximation de Prix Moov\'In'))
            ->setFrom(array($devis['email'] => 'Moovin devis approximative'))
            ->setTo('dheker.hamami@gmail.com')
            ->setBody(
                $this->renderView(
                    'emails/devis.html.twig',
                    array(
                        'email' => $devis['email'],
                        'nature' => $devis['nature'],
                        'area' => $devis['area'],
                        'showcase' => $devis['showcase'],
                        'google' => $devis['google'],
                        'shooting' => $devis['shooting'],
                        'support' => $devis['support'],
                        'price' => $devis['price'],
                    )
                ),
                'text/html'
            );

        $mailer->send($message);

        return new JsonResponse('email send', 200);
    }

}