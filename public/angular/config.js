"use strict";
var app = angular.module("moovinApp", ['ngResource']);
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});
