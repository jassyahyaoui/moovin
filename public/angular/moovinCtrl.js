app.controller("moovinCtrl", function ($scope, $resource) {
    $scope.current = '#home';
    $scope.UrlClick = function ($id) {
        $scope.current = $id;
    };

    $scope.process = true;
    $scope.sendEmail = function () {
        var email = $resource(Routing.generate('moovin_contact_email'));
        email.save($scope.contact,
            function (successResult) {
                $scope.process = false;
                $scope.contact = null;
            },
            function (errorResult) {
                alert('email does not been sended');
            });
        $scope.process = true;
    }

    $scope.sendDevis = function () {
        var devis = $resource(Routing.generate('moovin_devist_email'));
        devis.save($scope.devis,
            function (successResult) {
            },
            function (errorResult) {
                alert('email does not been sended');
            });
    }

    $scope.devis = {
        email: null,
        nature: null,
        area: 50,
        showcase: false,
        google: false,
        shooting: false,
        support: false
    };

    $scope.value = 0;
    $scope.value = 0;
    $scope.calculate = function () {
        if (($scope.devis.email != null) && ($scope.devis.nature != null) && ($scope.devis.area != null)) {
            switch (true) {
                case $scope.devis.area > 49 && $scope.devis.area <= 200 :
                    $scope.price = 6;
                    break;
                    case $scope.devis.area > 200 && $scope.devis.area <= 300 :
                    $scope.price = 5;
                    break;
                    case $scope.devis.area > 300 && $scope.devis.area <= 500 :
                    $scope.price = 4;
                    break;
                    case $scope.devis.area > 500 && $scope.devis.area <= 1000 :
                    $scope.price = 3;
                    break;
                    case $scope.devis.area > 1000 && $scope.devis.area <= 1500 :
                    $scope.price = 2;
                    break;
                    case $scope.devis.area > 1500 && $scope.devis.area <= 2000 :
                    $scope.price = 1.5;
                    break;
                    case $scope.devis.area > 2000 :
                        $scope.price = 1;
                        break;


            }
  /*  $scope.calculate = function () {
        if (($scope.devis.email != null) && ($scope.devis.nature != null) && ($scope.devis.area != null)) {
            switch (true) {
                case $scope.devis.area > 49 && $scope.devis.area <= 100 :
                    $scope.price = 6;
                    break;
                case $scope.devis.area > 100 && $scope.devis.area <= 200 :
                    $scope.price = 6;
                    break;
                case $scope.devis.area > 200 && $scope.devis.area <= 400 :
                    $scope.price = 6;
                    break;
                case $scope.devis.area > 400 && $scope.devis.area <= 600 :
                    $scope.price = 3;
                    break;
                case $scope.devis.area > 600 && $scope.devis.area <= 1000 :
                    $scope.price = 3;
                    break;
                case $scope.devis.area > 1000 && $scope.devis.area <= 2000 :
                    $scope.price = 1.5;
                    break;
                case $scope.devis.area > 2000 && $scope.devis.area <= 4000 :
                    $scope.price = 1;
                    break;
                case $scope.devis.area > 4000 && $scope.devis.area <= 8000 :
                    $scope.price = 1;
                    break;
                case $scope.devis.area > 8000 :
                    $scope.price = 1;
                    break;
            }
            */
            $scope.priceShowcase = 0;
            if ($scope.devis.showcase) {
                $scope.priceShowcase = 300;
            }
            $scope.priceShooting = 0;
            if ($scope.devis.shooting) {
                $scope.priceShooting = 300;
            }
            $scope.priceGoogle = 0;
            if ($scope.devis.google) {
                $scope.priceGoogle = 150;
            }
            $scope.priceSupport = 0;
            if ($scope.devis.support) {
                $scope.priceSupport = 150;
            }
            $scope.value = ($scope.devis.area * $scope.price)
                + $scope.priceShowcase
                + $scope.priceShooting
                + $scope.priceGoogle
                + $scope.priceSupport;
            $scope.devis.price = $scope.value;
            $scope.sendDevis();
        }
    }

});