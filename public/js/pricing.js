jQuery(document).ready(function ($) {
    function pricingSwitcher(elementCheck, elementParent, elementPricing) {
        elementParent.find('.pts-left,.pts-right').removeClass('pts-switch-active');
        elementPricing.find('.pts-switch-content-left,.pts-switch-content-right').addClass('hidden');

        if (elementCheck.filter(':checked').length > 0) {
            elementParent.find('.pts-right').addClass('pts-switch-active');
            elementPricing.find('.pts-switch-content-right').removeClass('hidden');
        } else {
            elementParent.find('.pts-left').addClass('pts-switch-active');
            elementPricing.find('.pts-switch-content-left').removeClass('hidden');
        }
    }

    $('.pts-switcher').each(function () {
        var element = $(this),
            elementCheck = element.find(':checkbox'),
            elementParent = $(this).parents('.pricing-tenure-switcher'),
            elementPricing = $(elementParent.attr('data-container'));

        pricingSwitcher(elementCheck, elementParent, elementPricing);

        elementCheck.on('change', function () {
            pricingSwitcher(elementCheck, elementParent, elementPricing);
        });
    });
});